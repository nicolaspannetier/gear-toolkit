#!/usr/bin/env sh

set -eu
unset CDPATH
cd "$( dirname "$0" )/../.."


USAGE="
Usage:
    $0 [OPTION...] [[--] PYTEST_ARGS...]

Runs all tests and linting.

Assumes running in test container or that gear_toolkit and all of its
dependencies are installed.

Options:
    -h, --help              Print this help and exit

    -s, --shell             Enter shell instead of running tests
    -l, --lint-only         Run linting only
    -d, --doc-only          Run sphinx-build doc only
    -L, --skip-lint         Skip linting
    -D, --skip-doc          Skip sphinx-build doc
    -- PYTEST_ARGS          Arguments passed to py.test

"


main() {
    export PYTHONDONTWRITEBYTECODE=1
    export PYTHONPATH=.
    local LINT_TOGGLE=
    local DOC_TOGGLE=

    while [ $# -gt 0 ]; do
        case "$1" in
            -h|--help)
                log "$USAGE"
                exit 0
                ;;
            -s|--shell)
                sh
                exit
                ;;
            -l|--lint-only)
                LINT_TOGGLE=true
                ;;
            -d|--doc-only)
                DOC_TOGGLE=true
                ;;
            -L|--skip-lint)
                LINT_TOGGLE=false
                ;;
            -D|--skip-doc)
                DOC_TOGGLE=false
                ;;
            --)
                shift
                break
                ;;
            *)
                break
                ;;
        esac
        shift
    done

    log "INFO: Cleaning pyc and previous coverage results ..."
    find . -type d -name __pycache__ -exec rm -rf {} \; || true
    find . -type f -name '*.pyc' -delete
    rm -rf .coverage htmlcov
    rm -rf docs/build

    if [ "$LINT_TOGGLE" != true -a "$DOC_TOGGLE" != true ]; then
        log "INFO: Running tests ..."
        pytest tests --exitfirst --cov=gear_toolkit --cov-report= "$@"

        log "INFO: Reporting coverage ..."
        local COVERAGE_ARGS="--skip-covered"
        coverage report --show-missing $COVERAGE_ARGS
        coverage html $COVERAGE_ARGS
    fi

    if [ "$DOC_TOGGLE" != false -a "$LINT_TOGGLE" != true ]; then
        log "INFO: Running sphinx-build ..."
        sphinx-build docs/source docs/build/html
    fi

    if [ "$LINT_TOGGLE" != false -a "$DOC_TOGGLE" != true ]; then
        log "INFO: Running pylint ..."
        pylint --rcfile tests/.pylintrc gear_toolkit

        log "INFO: Running pycodestyle ..."
        pycodestyle --max-line-length=150 --ignore=E402 gear_toolkit
    fi
}

log() {
    printf "\n%s\n" "$@" >&2
}


main "$@"
