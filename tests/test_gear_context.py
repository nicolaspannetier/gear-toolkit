import argparse
import json
import os
import sys
import tempfile
from unittest import mock

import pathlib
import pytest
from flywheel_bids import export_bids

import gear_toolkit
from gear_toolkit.gear_toolkit_context import convert_config_type


def test_convert_config_type():
    # test no type
    input_str = '4'
    output_val = convert_config_type(input_str)
    assert output_val == input_str
    assert type(output_val) == str

    # test str, string
    for input_str in ['4:string', '4:str']:
        output_val = convert_config_type(input_str)
        assert output_val == '4'
        assert type(output_val) == str

    # test booleans
    for input_str in ['false:boolean', 'False:boolean', 'FALSE:bool', 'true:bool', 'TRUE:boolean', 'true:bool']:
        output_val = convert_config_type(input_str)
        assert type(output_val) == bool
        if 'false' in input_str.lower():
            assert output_val == False
        elif 'true' in input_str.lower():
            assert output_val == True

    # test integers
    for input_str in ['4:number', '4:integer', '4:int']:
        output_val = convert_config_type(input_str)
        assert output_val == 4
        assert type(output_val) == int

    # test floats
    for input_str in ['4:float', '4.0:number']:
        output_val = convert_config_type(input_str)
        assert output_val == 4
        assert type(output_val) == float

    # test input_str not a string
    with pytest.raises(ValueError) as err:
        input_str = 4
        assert convert_config_type(input_str)
    assert str(err.value) == 'input_str 4 is not str'

    # test boolean input_str that is not true or false:
    with pytest.raises(ValueError) as err:
        input_str = "4:boolean"
        assert convert_config_type(input_str)
    assert str(err.value) == 'Cannot convert 4 to a boolean'

    # test unknown type_str
    with pytest.raises(ValueError) as err:
        input_str = '4:garbage'
        assert convert_config_type(input_str)
    assert str(err.value) == 'Unrecognized type_str: garbage'


def test_parse_context_args(args_file):
    # Test default
    args = gear_toolkit.gear_toolkit_context.parse_context_args(input_args=[])
    assert args == argparse.Namespace(api_key='', destination='aex:acquisition')

    # Test sys.argv behavior
    test_args = ['python3', 'run.py', '-d', '5e0612a6f999360027e1aa9d:project', '-garbage', 'garbage_value']
    with mock.patch.object(sys, 'argv', test_args):
        args = gear_toolkit.gear_toolkit_context.parse_context_args()
        assert args == argparse.Namespace(
            api_key='', destination='5e0612a6f999360027e1aa9d:project', garbage='garbage_value'
        )

    # Test input_args behavior
    test_args = ['python3', 'run.py', '-d', '5e0612a6f999360027e1aa9d:project', '-garbage', 'garbage_value']

    args = gear_toolkit.gear_toolkit_context.parse_context_args(test_args)
    assert args == argparse.Namespace(
        api_key='', destination='5e0612a6f999360027e1aa9d:project', garbage='garbage_value'
    )

    # Test text file behavior
    args_filepath = args_file('parse_args1')
    args = gear_toolkit.gear_toolkit_context.parse_context_args([f'@{args_filepath}'])
    assert args == argparse.Namespace(
        api_key='', destination='5e0612a6f999360027e1aa9d:project', garbage='garbage_value'
    )


def test_context_config_from_args(config_file):
    # Test that config is not be changed with default arguments
    config_path = config_file('config1')
    with open(config_path, 'r') as fp:
        original_config = json.load(fp)
    gt_context = gear_toolkit.GearToolkitContext(config_path=config_path, input_args=[], tempdir=True)
    assert gt_context.config_json == original_config

    # Test that destination is updated in the config
    gt_context.config_from_args(input_args=['-d', '9876543210:project'])
    assert gt_context.config_json['destination'] == {'id': '9876543210', 'type': 'project'}

    # Test add config value:
    gt_context.config_from_args(input_args=['--garbage1', '1:int'])
    assert gt_context.config.get('garbage1') == 1


def test_context_default_properties():
    cwd = pathlib.Path(os.getcwd())
    with gear_toolkit.GearToolkitContext(input_args=[]) as gtk_context:
        assert gtk_context.config == {}
        assert gtk_context.destination == {'id': 'aex', 'type': 'acquisition'}
        assert gtk_context.work_dir == cwd/'work'
        assert gtk_context.output_dir == cwd/'output'
        with gtk_context.open_output('spam.txt', 'a') as appnd:
            appnd.close()
        # Cleanup directories
        (gtk_context.output_dir/'spam.txt').unlink()
        gtk_context.output_dir.rmdir()
        gtk_context.work_dir.rmdir()


def test_context_inputs(config_file):
    config_path = config_file('config1')
    with open(config_path, 'r') as fp:
        config_dict = json.load(fp)
    with gear_toolkit.GearToolkitContext(config_path=config_path, input_args=[], tempdir=True) as gtk_context:
        # test get input
        assert gtk_context.get_input('dicom') == config_dict['inputs'].get('dicom')

        # test valid get_input_path
        assert gtk_context.get_input_path('dicom') == config_dict['inputs']['dicom']['location']['path']

        # test None get_input_path
        assert gtk_context.get_input_path('spam') == None

        # test invalid get_input_path
        with pytest.raises(ValueError) as err:
            assert gtk_context.get_input_path('api-key')
        assert str(err.value) == 'The specified input api-key is not a file'

        # test valid context
        context_val = gtk_context.get_context_value('classifications')
        assert context_val == "spam"

        # test undefined context
        context_val = gtk_context.get_context_value('doesnt_exist')
        assert context_val == None

        # test invalid context
        with pytest.raises(ValueError) as err:
            assert gtk_context.get_context_value('api-key')
        assert str(err.value) == 'The specified input api-key is not a context input'

        # test invalid api-key
        assert gtk_context.get_input('api-key')['key'] == 'my_api_key'
        assert gtk_context.client == None
        gtk_context.config_json['inputs'].pop('api-key')

        # test no api-key
        with mock.patch('flywheel.client.get_api_key_from_cli', return_value=None):
            assert gtk_context.get_input('api-key') == None
            assert gtk_context.client == None

        # test restore api-key
        gtk_context.config_from_args(input_args=['-k', 'my_api_key'])
        assert gtk_context.get_input('api-key')['key'] == 'my_api_key'

        # test add input
        input_path = gtk_context.work_dir/'spam.txt'
        input_path.touch()
        gtk_context.config_from_args(input_args=['--input1', input_path])
        assert gtk_context.get_input_path('input1') == str(input_path)


def test_context_open_output():

    with gear_toolkit.GearToolkitContext(input_args=[], tempdir=True) as gtk_context:
        gtk_context.config_json['inputs'] = {
            "dicom": {
                "base": "file",
                "hierarchy": {
                    "id": "aex",
                    "type": "acquisition"
                },
                "location": {
                    "name": "T1w_MPR.dcm.zip",
                    "path": gtk_context.output_dir/'T1w_MPR.dcm.zip'
                }
            }
        }
        # test invalid open_input
        with pytest.raises(ValueError) as err:
            assert gtk_context.open_input('spam')
        assert str(err.value) == 'Input spam is not defined in the config.json'

        # test non-existent open_input
        input_path = gtk_context.get_input_path('dicom')
        with pytest.raises(FileNotFoundError) as err:
            assert gtk_context.open_input('dicom')
        assert str(err.value) == f'Input dicom does not exist at {input_path}'

        # test valid open_input
        pathlib.Path(input_path).touch()
        with gtk_context.open_input('dicom') as rdr:
            rdr.close()


def test_context_init_logging():
    with gear_toolkit.GearToolkitContext(input_args=[], tempdir=True) as gtk_context:

        # test default
        default_config_name, update_config = gtk_context.init_logging()
        assert update_config == None
        assert default_config_name == 'info'

        # test debug
        default_config_name, _ = gtk_context.init_logging(default_config_name='debug')
        assert default_config_name == 'debug'
        gtk_context.config['debug_gear'] = True
        default_config_name, _ = gtk_context.init_logging()
        assert default_config_name == 'debug'

        # test manifest.custom.log_config
        gtk_context.manifest = {'custom': {'log_config': {'disable_existing_loggers': True}}}
        _, update_config = gtk_context.init_logging()
        assert update_config == {'disable_existing_loggers': True}


def test_context_log_config(config_file):
    config_path = config_file('config1')
    with gear_toolkit.GearToolkitContext(config_path=config_path, input_args=[], tempdir=True) as gtk_context:
        gtk_context.log_config()


def test_context_metadata():
    with gear_toolkit.GearToolkitContext(input_args=[], tempdir=True) as gtk_context:
        # test update_file_metadata
        file_update1 = gtk_context.update_file_metadata('spam.txt', {'type': 'dicom'}, info={'spam': 'eggs'})
        assert file_update1 == {'type': 'dicom', 'info': {'spam': 'eggs'}}

        # test update existing file metadata
        file_update2 = gtk_context.update_file_metadata('spam.txt', {'type': 'nifti'})
        assert file_update2 == {'type': 'nifti'}
        file_update1.update(file_update2)

        # test update_container_metadata
        subject_update1 = gtk_context.update_container_metadata(
            'subject', {'strain': 'C10', 'species': 'mouse'}, info={'spam': 'eggs'}
        )
        assert subject_update1 == {'strain': 'C10', 'species': 'mouse', 'info': {'spam': 'eggs'}}
        # test update_destination metadata
        dest_update = gtk_context.update_destination_metadata(uid='python', info={'spam': 'eggs'})
        assert dest_update == {'uid': 'python', 'info': {'spam': 'eggs'}}

        # test metadata
        subject_update2 = gtk_context.update_container_metadata('subject', {'strain': 'C6'})
        subject_update1.update(subject_update2)
        file_update1.update({'name': 'spam.txt'})
        dest_update.update({'files': [file_update1]})
        assert gtk_context._metadata == {
            'subject': subject_update1,
            'acquisition': dest_update
        }

        # test conflicting args/kwargs
        subject_update3 = gtk_context.update_container_metadata('subject', {'info': {'eggs': 'spam'}},
                                                                info={'spam': 'eggs'})
        assert subject_update3 == {'info': {'spam': 'eggs'}}

        # test bad args
        with pytest.raises(TypeError) as err:
            assert gtk_context.update_container_metadata('subject', 4)
        assert str(err.value) == f'*args must be of type dict, got {type(4)} for {4}'


def test_context_load_download_bids():
    with gear_toolkit.GearToolkitContext(input_args=[], tempdir=True) as gtk_context:
        from flywheel_bids.export_bids import download_bids_dir
        assert gtk_context._load_download_bids() == download_bids_dir

    # test ImportError
    with mock.patch.dict('sys.modules', {'flywheel_bids.export_bids': None}):
        with pytest.raises(RuntimeError) as err:
            assert gtk_context._load_download_bids()
        assert str(err.value) == 'Unable to load flywheel-bids package, make sure it is installed!'


def test_context_json_decode():
    with tempfile.TemporaryDirectory() as tempdir:
        dir_path = pathlib.Path(tempdir)
        text_file_path = dir_path/'spam.txt'
        text_file_path.touch()
        text_file_path.write_text('Lorem ipsum dolor sit amet')
        with pytest.raises(RuntimeError) as err:
            assert gear_toolkit.GearToolkitContext(input_args=[], tempdir=True, config_path=text_file_path)
        assert str(err.value) == f'Cannot parse {text_file_path} as JSON.'


def test_context_validate_bids_download():
    # mock client as a dictionary
    mock_acquisition = {
        'session': 'mock_session_id',
        'parents': {'acquisition': None,
                    'analysis': None,
                    'group': 'mock_group_id',
                    'project': 'mock_project_id',
                    'session': 'mock_session_id',
                    'subject': 'mock_subject_id'}
    }
    mock_client = {'aex': mock_acquisition}

    with gear_toolkit.GearToolkitContext(input_args=[], tempdir=True) as gtk_context:
        gtk_context._client = mock_client
        # test valid session command
        parent_id, target_dir, download_bids_dir, kwargs = gtk_context._validate_bids_download(
            container_type='session', target_dir=None, sessions=None, subjects=['mock_subject_id']
        )
        expected_target_dir = gtk_context.work_dir/'bids'
        assert parent_id == 'mock_session_id'
        assert target_dir == expected_target_dir
        assert download_bids_dir == export_bids.download_bids_dir
        assert kwargs == {'subjects': ['mock_subject_id']}

        # test valid subject command with str target_dir
        parent_id, target_dir, download_bids_dir, kwargs = gtk_context._validate_bids_download(
            container_type='project', target_dir=f'{gtk_context.work_dir}/bids', sessions=['mock_session_id'], subjects=None
        )
        assert parent_id == 'mock_project_id'
        assert target_dir == (gtk_context.work_dir/'bids')
        assert kwargs == {'sessions': ['mock_session_id']}

        # test valid subject command with pathlib.Path target_dir
        parent_id, target_dir, download_bids_dir, kwargs = gtk_context._validate_bids_download(
            container_type='project', target_dir=gtk_context.work_dir/'bids', sessions=['mock_session_id'],
            subjects=None
        )
        assert parent_id == 'mock_project_id'
        assert target_dir == (gtk_context.work_dir / 'bids')
        assert kwargs == {'sessions': ['mock_session_id']}

        # test bad dir type
        with pytest.raises(TypeError) as err:
            assert gtk_context._validate_bids_download(
                container_type='project', target_dir=8, sessions=['mock_session_id'],
                subjects=None
            )
        # test no parent id
        with pytest.raises(RuntimeError) as err:
            assert gtk_context._validate_bids_download(
                container_type='acquisition', target_dir=None, sessions=['mock_session_id'],
                subjects=None
            )
