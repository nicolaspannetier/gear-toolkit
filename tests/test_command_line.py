import logging
import tempfile
import pytest
from subprocess import SubprocessError
from gear_toolkit.command_line import build_command_list, exec_command
from gear_toolkit.logging.templated import configure_logging
from test_logging import template_helper_fn

log = logging.getLogger(__name__)


def test_build_command_list():
    # Multiple ASSERT statements can be made OR...

    # test single-character no value
    command = ['ls']
    params = {'l': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)
    result1 = ['ls', '-l', '-a', '-h']
    assert command1 == result1

    # test with single-character with value
    command = ['head']
    params = {'n': '5'}
    command2 = build_command_list(command, params)
    result2 = ['head', '-n', '5']
    assert command2 == result2

    # test with boolean value
    command = ['head']
    params = {'n': '5', 'i': True, 'Bool': True}
    command2 = build_command_list(command, params)
    result2 = ['head', '-n', '5', '-i', '--Bool']
    assert command2 == result2

    # test with multi-character no value and with value
    command = ['du']
    params = {'a': '', 'human-readable': '', 'max-depth': 3}
    command3 = build_command_list(command, params)
    result3 = ['du', '-a', '--human-readable', '--max-depth=3']
    assert command3 == result3

    # test without keys
    command = ['nonsense']
    params = {'File1': 'this.txt', 'File2': 'that.txt'}
    command4 = build_command_list(command, params, include_keys=False)
    result4 = ['nonsense', 'this.txt', 'that.txt']
    assert command4 == result4

# def exec_command(command, dry_run=False, environ={}, shell=False,
#                 stdout_msg=None, cont_output=False):


def test_exec_command(caplog):
    template_helper_fn()
    command = ['ls']
    params = {'l': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)
    exec_command(command1)
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_fail(caplog):
    template_helper_fn()
    command = ['ls']
    params = {'ldfs': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)

    with pytest.raises(RuntimeError) as err:
        assert exec_command(command1)
    assert 'The following command has failed: ' in str(err.value)


def test_exec_command_dry_run(caplog):
    template_helper_fn()
    command = ['ls']
    params = {'l': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)
    exec_command(command1, dry_run=True)
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_shell(caplog):
    template_helper_fn()
    command = ['ls']
    params = {'l': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)
    with tempfile.NamedTemporaryFile() as ntf:
        params = {'redirect': '>>', 'file': ntf.name}
        command1 = build_command_list(command1, params, include_keys=False)
        exec_command(command1, shell=True)
        for record in caplog.records:
            assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_stdout_msg(caplog):
    template_helper_fn()
    command = ['ls']
    params = {'l': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)
    exec_command(command1, stdout_msg='No StdOut!!!')
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")


def test_exec_command_cont_output(caplog):
    template_helper_fn()
    command = ['ls']
    params = {'l': '', 'a': '', 'h': ''}
    command1 = build_command_list(command, params)
    exec_command(command1, cont_output=True)
    for record in caplog.records:
        assert record.levelname not in ("CRITICAL", "ERROR")
