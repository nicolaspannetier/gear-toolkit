import os
import pytest
import shutil
import tempfile

DATA_ROOT = os.path.join('tests', 'data')


@pytest.fixture(scope='function')
def args_file():
    def get_args_file(name):
        return os.path.join(DATA_ROOT, name + '.txt')
    return get_args_file


@pytest.fixture(scope='function')
def config_file():
    def get_config_file(name):
        return os.path.join(DATA_ROOT, name + '.json')
    return get_config_file