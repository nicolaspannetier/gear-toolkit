"""Unit test for running tree system command"""

from os import chdir
import logging
from pathlib import Path

from gear_toolkit.bids.tree import tree_bids


def test_tree_bids_basic_results_works(caplog, tmp_path):
    """Make sure tree output actually happens."""

    caplog.set_level(logging.DEBUG)

    bids_path = Path('work/bids')

    the_temp_dir = tmp_path/Path(bids_path)
    the_temp_dir.mkdir(parents=True)
    chdir(str(tmp_path))

    Path('work/bids/adir').mkdir()
    Path('work/bids/adir/anotherfiile.json').touch()
    Path('work/bids/afile.txt').touch()
    Path('work/bids/anotherdir').mkdir()
    Path('work/bids/anotherdir/README.md').touch()

    tree_bids(bids_path, 'tree_out')

    with open('tree_out.html') as tfp:
        html = tfp.read().split('\n')

    assert len(caplog.records) == 2
    assert Path('tree_out.html').exists()
    assert html[8] == 'work/bids/'  # has trailing '/'
    assert html[14] == '2 directories, 3 files'
    print(caplog.records)
    assert caplog.records[1].message == 'Wrote "tree_out.html"'
