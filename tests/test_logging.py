import pytest
import logging
from gear_toolkit.logging import templated

log = logging.getLogger(__name__)


@pytest.fixture(params=['debug', 'info', 'warning', 'error', 'critical'])
def get_log_message(request):
    return f'log.{request.param}("Test a {request.param} entry")'


def template_helper_fn(default_config_name='info', update_config=None):
    """
    template_helper_fn takes the same parameters as the
    templated.configure_logging() function.  It provides the
    LogCaptureHandler to the log that was instantiated from the template.
    This assists in running the caplog fixture. In normal operation, this will
    not be required.

    Args:
        default_config_name ([type], optional):  Defaults to logging.INFO.
        update_config ([type], optional): [description]. Defaults to None.
    """

    if len(logging.root.handlers) == 1:
        root_handler = logging.root.handlers[0]
    else:
        root_handler = logging.root.handlers[1]
        logging.root.removeHandler(root_handler)

    templated.configure_logging(
        default_config_name=default_config_name,
        update_config=update_config
        )
    root_handler.setLevel(logging.root.level)
    root_handler.setFormatter(logging.root.handlers[0].formatter)
    logging.root.addHandler(root_handler)


def test_templated_info_multiple(get_log_message, caplog):
    with caplog.at_level(logging.INFO):
        template_helper_fn()
        eval(get_log_message)
        for record in caplog.records:
            assert record.levelno >= logging.root.level

        if 'debug' in get_log_message:
            assert len(caplog.records) == 0


def test_templated_wrong_level(get_log_message, caplog):
    with caplog.at_level(logging.DEBUG):
        template_helper_fn(default_config_name='debugs')
        eval(get_log_message)
        for record in caplog.records:
            assert logging.root.level == 20
            assert record.levelno >= logging.root.level


def test_templated_debug_multiple(get_log_message, caplog):
    with caplog.at_level(logging.DEBUG):
        template_helper_fn(default_config_name='debug')
        eval(get_log_message)
        for record in caplog.records:
            assert record.levelno >= logging.root.level


def test_templated_set_config(get_log_message, caplog):
    with caplog.at_level(logging.INFO):
        config = {
            "formatters": {
                "gear": {
                    "format": "[TEST] %(levelname)4s %(message)s"
                }
            }
        }

        template_helper_fn(update_config=config)
        eval(get_log_message)

        for record in caplog.records:
            assert record.levelno >= logging.root.level
        if 'debug' not in get_log_message:
            assert '[TEST]' in caplog.text
        else:
            assert len(caplog.records) == 0
