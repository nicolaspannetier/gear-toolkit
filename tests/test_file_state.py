import pytest
import tempfile
import sys
import os
import inspect
from pathlib import Path

currentdir = Path(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = currentdir.parent
sys.path.insert(0,parentdir.as_posix())

from gear_toolkit.file_state import file_state


@pytest.fixture
def create_test_files():
    with tempfile.TemporaryDirectory() as tmp_folder:
        
        out_dir = Path(tmp_folder,'tmp')
        out_dir.mkdir(parents=True, exist_ok=True)
        
        file = Path(out_dir, 'test_file.ext1.ext2')
        file.touch()
        
        yield file
        
@pytest.fixture
def donot_create_test_files():
    with tempfile.TemporaryDirectory() as tmp_folder:
        out_dir = Path(tmp_folder, 'tmp')
        out_dir.mkdir(parents=True, exist_ok=True)
        file = Path(out_dir, 'non_test_file.ext1.ext2')
        if file.exists():
            os.remove(file)
            
        yield file
    

def test_exists_present_and_expected(create_test_files):
    file = create_test_files
    assert file_state(file,is_expected=True, exception_on_error=False) is True
    
def test_exists_present_and_unexpected_noexcept(create_test_files):
    file = create_test_files
    assert file_state(file,is_expected=False, exception_on_error=False) is False

def test_exists_present_and_unexpected_except(create_test_files):
    file = create_test_files
    with pytest.raises(Exception):
        file_state(file,is_expected=False, exception_on_error=True) is False
    
def test_exists_absent_and_expected_noexcept(donot_create_test_files):
    file = donot_create_test_files
    assert file_state(file,is_expected=True, exception_on_error=False) is False
    
def test_exists_absent_and_expected_except(donot_create_test_files):
    file = donot_create_test_files
    with pytest.raises(Exception):
        file_state(file,is_expected=True, exception_on_error=True)
        
def test_exists_absent_and_unexpected(donot_create_test_files):
    file = donot_create_test_files
    assert file_state(file,is_expected=False, exception_on_error=False) is True
    
def test_exists_single_ext_correct(create_test_files):
    file = create_test_files
    assert file_state(file,is_expected=True,ext='.ext2') is True

def test_exists_double_ext_correct(create_test_files):
    file = create_test_files
    assert file_state(file,is_expected=True,ext='.ext1.ext2') is True
    
def test_exists_long_ext_exception(create_test_files):
    file = create_test_files
    with pytest.raises(Exception):
        file_state(file,is_expected=True,ext='extension.longer.than.file')

def test_exists_ext_incorrect_noexcept(create_test_files):
    file = create_test_files
    assert file_state(file,is_expected=True,ext='.bad',exception_on_error=False) is True 
    
def test_exists_ext_incorrect_except(create_test_files):
    file = create_test_files
    with pytest.raises(Exception):
        file_state(file,is_expected=True,ext='.bad',exception_on_error=True) is True 