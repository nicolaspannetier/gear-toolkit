import pytest
import tempfile
import glob
from unittest import mock
import json
import shutil
import os
import os.path as op
from pathlib import Path
from gear_toolkit.zip_tools import zip_output, zip_list, unzip_all, unzip_config
from zipfile import ZipFile


@pytest.fixture
def create_temporary_files_and_dirs():
    with tempfile.TemporaryDirectory() as tmp_folder:
        (Path(tmp_folder)/'tmp'/'subdir').mkdir(parents=True, exist_ok=True)
        fs_list = ['tmp/subdir']
        file_list = [
            Path('tmp') / 'file1.txt',
            Path('tmp') / 'file2.txt',
            Path('tmp') / 'file3.txt'
        ]
        for fl in file_list:
            (Path(tmp_folder) / fl).touch()
        fs_list += file_list
        config = {'elements': 5, 'depth': 6, 'name': 'George'}
        config_dest = Path('tmp') / 'this_config.json'
        with open(Path(tmp_folder) / config_dest, 'w') as fid:
            json.dump(config, fid)
        fs_list.append(config_dest)

        yield tmp_folder, sorted(map(str, fs_list))


@pytest.fixture
def create_temporary_zip(create_temporary_files_and_dirs):
    tmpdir, file_list = create_temporary_files_and_dirs
    with tempfile.NamedTemporaryFile(mode='w', suffix='.zip') as zipfile:
        shutil.make_archive(zipfile.name.split('.zip')[0],
                            'zip', root_dir=tmpdir)
        yield zipfile.name, file_list


def test_zip_list_should_open_zipfile():
    with mock.patch('gear_toolkit.zip_tools.ZipFile') as MockZipFile:
        zip_list('mock.zip')
        MockZipFile.assert_called_with('mock.zip')


def test_unzip_all_with_normal_condition(tmpdir, create_temporary_zip):
    zipfile, file_list = create_temporary_zip

    unzip_all(zipfile, str(tmpdir))
    unzip_list = sorted(
        [
            fl.split(str(tmpdir)+'/')[1]
            for fl in glob.glob(str(tmpdir/'*'/'*'), recursive=True)
        ]
    )
    assert unzip_list == file_list


def test_unzip_all_with_dry_run(tmpdir, create_temporary_zip):
    zipfile, _ = create_temporary_zip

    unzip_all(zipfile, tmpdir, dry_run=True)
    assert not (tmpdir/'tmp').exists()


def test_unzip_config(create_temporary_zip):
    zipfile, _ = create_temporary_zip

    # grab the known config from within
    config = unzip_config(zipfile)
    assert len(config.keys()) == 1
    assert len(config['config'].keys()) == 3

    # give a search string that does not match
    try:
        config = unzip_config(zipfile, search_str='bad')
    except Exception as e:
        assert 'not find a valid configuration' in str(e)


def test_zip_list(create_temporary_zip):
    zipfile, file_list = create_temporary_zip
    file_list.remove('tmp/subdir')

    # test our inputs against outputs
    zip_file_list = zip_list(zipfile)
    assert zip_file_list == file_list


def test_zip_output(tmpdir, create_temporary_files_and_dirs):
    root_dir, file_list = create_temporary_files_and_dirs
    zipfile = op.join(tmpdir, 'example.zip')
    if 'tmp/subdir' in file_list:
        file_list.remove('tmp/subdir')
    shutil.copytree(root_dir, tmpdir/'new')

    # Test zipping directory
    zip_output(tmpdir/'new', 'tmp', zipfile)
    zip_file_list = zip_list(zipfile)
    assert zip_file_list == sorted(file_list)


def test_zip_output_bad_root(tmpdir, create_temporary_files_and_dirs):
    root_dir, file_list = create_temporary_files_and_dirs
    zipfile = op.join(tmpdir, 'example.zip')
    if 'tmp/subdir' in file_list:
        file_list.remove('tmp/subdir')
    shutil.copytree(root_dir, tmpdir/'new')

    # Test zipping directory
    try:
        zip_output(tmpdir/'news', 'tmp', zipfile)
    except Exception as e:
        assert isinstance(e, FileNotFoundError)


def test_zip_output_dry_run(tmpdir, create_temporary_files_and_dirs):
    root_dir, _ = create_temporary_files_and_dirs
    zipfile = op.join(tmpdir, 'example.zip')
    shutil.copytree(root_dir, tmpdir / 'new')
    zip_output(tmpdir/'new', 'tmp', zipfile, dry_run=True)
    assert not op.exists(zipfile)


def test_zip_output_file_exclusion(tmpdir, create_temporary_files_and_dirs):
    root_dir, file_list = create_temporary_files_and_dirs
    zipfile = op.join(tmpdir, 'example.zip')
    exclude_list = ['tmp/file1.txt', 'tmp/file2.txt']
    if 'tmp/subdir' in file_list:
        file_list.remove('tmp/subdir')
    shutil.copytree(root_dir, tmpdir / 'new')

    zip_output(tmpdir/'new', 'tmp', zipfile, exclude_files=exclude_list)
    zip_file_list = zip_list(zipfile)
    assert set(zip_file_list) == set(file_list).difference(set(exclude_list))
