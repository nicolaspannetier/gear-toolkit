"""Unit test for installing Freesurfer file.
"""

import logging
from pathlib import Path
from unittest.mock import Mock, patch
import pytest

from gear_toolkit.license.freesurfer import find_freesurfer_license

# fake freesurfer license with spaces for \n
FS_STR = "name@flywheel.io 93241 *X9YoVve9t72.  XSslwsL42lwtv"

FAKE_PARENTS = {'parents': {'project': '5db0759469d4f3001f16e9c1'}}

FAKE_PROJECT = {'info': {'FREESURFER_LICENSE': FS_STR}}

FAKE_DESTINATION = {'type': 'analysis', 'id': '5e1dc6954d3a210022725d0f'}


def test_fs_license_in_input_works(tmp_path):
    """Freesurfer license string as an input file"""

    with patch('flywheel.gear_context.GearContext') as mock_context:

        fs_license_dir = Path(tmp_path)/'opt/freesurfer'
        fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir/'license.txt'

        input_dir = Path(tmp_path)/'input'
        input_dir.mkdir(parents=True)
        input_license = input_dir/'license.txt'

        # install fake license there
        with open(input_license, 'w') as ilp:
            ilp.write('\n'.join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        find_freesurfer_license(mock_context, fs_license)

        with open(fs_license) as flp:
            whole_file = flp.read()

        assert whole_file == '\n'.join(FS_STR.split())


def test_fs_license_in_input_name_different_detected(tmp_path, caplog):
    """Freesurfer license string as an input file"""

    caplog.set_level(logging.DEBUG)

    with patch('flywheel.gear_context.GearContext') as mock_context:

        fs_license_dir = Path(tmp_path)/'opt/freesurfer'
        fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir/'horsepucky.txt'

        input_dir = Path(tmp_path)/'input'
        input_dir.mkdir(parents=True)
        input_license = input_dir/'license.txt'

        # install fake license there
        with open(input_license, 'w') as ilp:
            ilp.write('\n'.join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        find_freesurfer_license(mock_context, fs_license)

        assert 'Freesurfer license file is usually license.txt, not' in \
                caplog.records[1].message


def test_fs_path_does_not_exist_detected(tmp_path, caplog):
    """Freesurfer license string as an input file"""

    caplog.set_level(logging.DEBUG)

    with patch('flywheel.gear_context.GearContext') as mock_context:

        fs_license_dir = Path(tmp_path)/'opt/freesurfer'
        # fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir/'license.txt'

        input_dir = Path(tmp_path)/'input'
        input_dir.mkdir(parents=True)
        input_license = input_dir/'license.txt'

        # install fake license there
        with open(input_license, 'w') as ilp:
            ilp.write('\n'.join(FS_STR.split()))

        mock_context.get_input_path = Mock(return_value=input_license)

        find_freesurfer_license(mock_context, fs_license)

        assert 'Had to make freesurfer license path' in \
                caplog.records[1].message


def test_fs_license_in_context_works(tmp_path, caplog):
    """Freesurfer license string in gear context
    """

    caplog.set_level(logging.DEBUG)

    with patch('flywheel.gear_context.GearContext') as mock_context:

        fs_license_dir = Path(tmp_path)/'opt/freesurfer'
        # fs_license_dir.mkdir(parents=True)
        fs_license = fs_license_dir/'license.txt'

        # pretend no input file was provided
        mock_context.get_input_path = Mock(return_value=None)

        # fake freesurfer license of the proper form
        mock_context.config = {}
        with patch.dict(mock_context.config,
                        {'gear-FREESURFER_LICENSE': FS_STR}):

            find_freesurfer_license(mock_context, fs_license)

            with open(fs_license) as flp:
                whole_file = flp.read()

            assert whole_file == '\n'.join(FS_STR.split())
            assert 'Created directory' in caplog.records[2].message


def test_fs_license_in_info_works(tmp_path, caplog):
    """Freesurfer license string in project info metadata
    """

    caplog.set_level(logging.DEBUG)

    with patch('flywheel.gear_context.GearContext') as mock_context:

        fs_license_path = Path(tmp_path)/'license.txt'

        # pretend no input file was provided
        mock_context.get_input_path = Mock(return_value=None)

        # license is not in (empty) config
        mock_context.config = {}

        mock_context.destination = FAKE_DESTINATION

        with patch('flywheel.client.Client') as mock_client:
            mock_context.client = mock_client

            mock_client.get_analysis = Mock(return_value=FAKE_PARENTS)

            mock_client.get_project = Mock(return_value=FAKE_PROJECT)

            find_freesurfer_license(mock_context, fs_license_path)

            with open(fs_license_path) as flp:
                whole_file = flp.read()

            assert whole_file == '\n'.join(FS_STR.split())


def test_fs_license_not_found_exception(caplog):
    """Freesurfer license string not found anywhere"""

    caplog.set_level(logging.DEBUG)

    with patch('flywheel.gear_context.GearContext') as mock_context:

        # a path that does not exist
        fs_license_path = Path('/opt/freesurfer/license.txt')

        # pretend no input file was provided
        mock_context.get_input_path = Mock(return_value=None)

        # license is not in (empty) config
        mock_context.config = {}

        mock_context.destination = FAKE_DESTINATION

        with patch('flywheel.client.Client') as mock_client:
            mock_context.client = mock_client

            mock_client.get_analysis = Mock(return_value=FAKE_PARENTS)

            # no license in project info either
            mock_client.get_project = Mock(return_value=
                                           {'info': {'not_here': None}})

            with pytest.raises(FileNotFoundError) as excinfo:

                find_freesurfer_license(mock_context, fs_license_path)

            assert 'Looking for Freesurfer license' in caplog.records[0].message
            assert 'Could not find FreeSurfer license' in str(excinfo.value)
            assert '/opt/freesurfer/license.txt' in str(excinfo.value)
