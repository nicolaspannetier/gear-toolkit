API
***

Below is a description of the flywheel gear-toolkit interface

bids
----
.. automodule:: gear_toolkit.bids
   :members:
   :undoc-members:
   :show-inheritance:

validate
^^^^^^^^
.. automodule:: gear_toolkit.bids.validate
   :members:
   :undoc-members:
   :show-inheritance:

tree
^^^^
.. automodule:: gear_toolkit.bids.tree
   :members:
   :undoc-members:
   :show-inheritance:

command_line
------------
.. automodule:: gear_toolkit.command_line
   :members:
   :undoc-members:
   :show-inheritance:

gear_toolkit_context
--------------------
.. automodule:: gear_toolkit.gear_toolkit_context
   :members:
   :undoc-members:
   :show-inheritance:

license
-------
.. automodule:: gear_toolkit.license
   :members:
   :undoc-members:
   :show-inheritance:

Freesurfer
^^^^^^^^^^
.. automodule:: gear_toolkit.license.freesurfer
   :members:
   :undoc-members:
   :show-inheritance:

logging
-------
.. automodule:: gear_toolkit.logging.templated
   :members:
   :undoc-members:
   :show-inheritance:


zip_tools
---------
.. automodule:: gear_toolkit.zip_tools
   :members:
   :undoc-members:
   :show-inheritance:

file_state
----------
.. automodule:: gear_toolkit.file_state
   :members:
   :undoc-members:
   :show-inheritance:
