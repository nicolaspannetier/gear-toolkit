Gear Toolkit Context
********************
The GearToolkitContext is an iteration on the Flywheel SDK's GearContext (``flywheel.GearContext``).

The :class:`gear_toolkit.gear_toolkit_context.GearToolkitContext` class provides a simplified interface for performing
common tasks in the lifecycle of a gear, such as accessing input files, configuration values, logging messages,
the gear manifest, accessing the SDK client, and writing to the output folder.

Environment
===========
A notable difference between the ``flywheel.GearContext`` and ``gear_toolkit.GearToolkitContext`` is the ability
to generate a config.json dictionary if none is found in the current working directory. Thus, it is recommended that the
gear's Docker image sets flywheel/v0 as the working directory, for example:

.. code-block:: DOCKER

    ENV FLYWHEEL="/flywheel/v0"
    # <other Dockerfile lines>
    WORKDIR $FLYWHEEL

When a gear is being executed outside of Flywheel, the user may provide config values and arguments by adding a
``--<config key or input key> <config value or input path>`` for each argument to the terminal run command.
These can also be provided via a newline(``\n``)-delimited text file by adding a ``@/path/to/text/file`` to the terminal
run command. Inputs are discerned on the basis that ``os.path.isfile(<config value or input path>)`` evaluates as
``True``. All other ``--`` prefaced arguments will be added to the config.config dictionary.

Destination can also be specified with ``-d <container id>:<container type>``

These arguments will only override config.json (if it exists) values when the keys do not already exist in the file.

For example, given a run.py python script of:

.. code-block:: python

    import gear_toolkit

    with gear_toolkit.GearToolkitContext() as gtk_context:
        config_json = gtk_context.config_json

A user may execute the following from a terminal to add ``"option1":1`` to the config.json config and an input dictionary
to inputs for ``input1`` :

.. code-block::

    python3 run.py --file_input1 /path/to/existing/input/file.txt --option1 1:integer -d 5e0612a6f999360027e1aa9d:project


Which would create the following config_json dictionary (provided that `/path/to/existing/input/file.txt` is a file that exists):

.. code-block:: json

    {
        "config": {"option1": 1},
        "inputs": {
            "file_input1": {
                "base": "file",
                "hierarchy": {"type": "acquisition", "id": "aex"},
                "location": {"name": "file.txt", "path": "/path/to/existing/input/file.txt"},
                "object": {
                    "classification": {"Intent": [], "Measurement": []},
                    "info": {},
                    "measurements": [],
                    "mimetype": "",
                    "modality": "",
                    "size": "<file_size>",
                    "tags": [],
                    "type": ""}
                }
            },
        "destination": {"id": "5e0612a6f999360027e1aa9d", "type": "project"}
    }


.. note:: in order for these config/input values to be parsed within the Flywheel gear environment, they will need to be
    added to the gear's manifest.json file. This functionality is intended to assist users with prototyping gears.



Setup
=====
The first step of using this interface is to create an instance of the GearToolkitContext class and,
if desired, initialize logging. You can also print your configuration to the log to
make troubleshooting easier during development:

.. code-block:: python

   import flywheel
   with flywheel.GearToolkitContext() as gtk_context:
      # Setup basic logging
      gtk_context.init_logging()

      # Log the configuration for this job
      gtk_context.log_config()

Accessing Config
================
Once initialized, the job configuration is accessible as a regular python ``dict`` from the gtk_context:

.. code-block:: python

   # Get the configured speed, with a default of 2
   my_speed = gtk_context.config.get('speed', 2)


Accessing Inputs
================
You can get the full path to a named input file, or open the file directly:

.. code-block:: python

   # Get the path to the input file named 'dicom'
   dicom_path = gtk_context.get_input_path('dicom')

   # Open the dicom file for reading
   with gtk_context.open_input('dicom', 'rb') as dicom_file:
      dicom_data = dicom_file.read()


Accessing the SDK Client
========================
If your gear is an SDK gear (that is, it has an api-key input), you can easily access
an instance of the Flywheel SDK Client:

.. code-block:: python

   # Lookup a project using the client
   project = gtk_context.client.lookup('my_group/Project 1')


If the gear script is executed in an environment where the user is logged in to the Flywheel CLI, the client will be
available. Otherwise ``--api_key <api key>`` can also be added to the command arguments to instantiate a client with a
specific key. If config.json is found, the api key in config.json will be used instead.

Downloading BIDS
================
If your project or session is fully curated for BIDS, you can download all or a portion
of the project or session to a working directory in BIDS layout.

.. code-block:: python

   # Download all files from the session in BIDS format
   # bids_path will point to the BIDS folder
   bids_path = gtk_context.download_session_bids()

   # Download anat and func files from the project in BIDS format
   # bids_path will point to the BIDS folder
   bids_path = gtk_context.download_project_bids(folders=['anat', 'func'])


Writing Outputs
===============
The path to the output directory is available as a variable on the gtk_context, and
helper methods exist for opening an output file for writing:

.. code-block:: python

   print('Output path: {}'.format(gtk_context.output_dir))

   # Open an output file for writing
   with gtk_context.open_output('out-file.dcm', 'wb') as f:
      f.write(dicom_data)


Writing Metadata
================
Occasionally it's useful to add or update metadata on the destination, one of the
parent containers, or files on the destination (including output files)

This can be done using the metadata helper functions. The metadata will be written either
when ``write_metadata()`` is called, or the gtk_context is exited (if using a ``with`` statement)

.. code-block:: python

   # Metadata will be written at exit of the "with" block,
   # unless an exception occurs
   with gear_toolkit.GearToolkitContext() as gtk_context:
      # Update the session label
      gtk_context.update_container_metadata('session', label='Session 1')

      # Update the destination (e.g. acquisition) label and timestamp
      updates = {
         'label': 'fMRI_Ret_bars',
         'timestamp': '2014-05-07T08:50:07+00:00'
      }
      gtk_context.update_destination_metadata(updates)

      # Set the modality and classification of an output file
      gtk_context.update_file_metadata('out-file.dcm', modality='MR', classification={
         'Intent': ['Functional'],
         'Measurement': ['T2*']
      })

