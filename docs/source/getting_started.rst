Getting Started
***************

Introduction
------------
``gear-toolkit`` is a python package maintained by `Flywheel <www.flywheel.io>`_.
It consists of a set of modules helping for Flywheel gears development.

License
-------
``gear-toolkit`` is developed under an MIT-based `license <https://https://gitlab.com/flywheel-io/public/gear-toolkit/blob/master/LICENSE>`_.

Installation
------------
The latest python package can be installed using pip:

.. code-block:: bash

	pip install gear_toolkit

The python wheel can also be downloaded from the `TBD <https://TBD>`_.

For development, please refer to the `README.md <https://gitlab.com/flywheel-io/public/gear-toolkit/blob/master/README.md>`_/


API Key
-------
Some modules in this package relies on `Flywheel SDK <https://flywheel-io.github.io/core/branches/master/python/index.html>`_
and requires a Flywheel API key. You can find and generate your key on the Flywheel
profile page. It will look like this:

.. image:: _static/images/api-key.png
