"""Software license handling utilities.

Software license files can be provided to the platform in various
ways.  Each module in this package handles the license for a particular
software package.
"""
