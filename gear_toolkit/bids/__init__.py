"""Brain Imaging Data Structure (BIDS) Utilities.

These modules provide common functions that are helpful when writing
gears that need to access BIDS formatted data.

More information on BIDS can be found at https://bids.neuroimaging.io/."""
