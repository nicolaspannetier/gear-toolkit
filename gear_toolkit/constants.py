"""A place for consolidating those darn globals..."""

FILE_OBJECT_DICT = {
    "classification": {
        "Intent": [],
        "Measurement": []
    },
    "info": {},
    "measurements": [],
    "mimetype": "",
    "modality": "",
    "size": 0,
    "tags": [],
    "type": ""
}
