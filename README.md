# Flywheel Gear Toolkit

**flywheel-gear-toolkit** is a library that provides tooling for developing Flywheel gears.

## Building

It's recommended that you use [pipenv](https://docs.pipenv.org/en/latest/) to manage dependencies. For example:
```
> python3 -m pip install pipenv
> pipenv install --dev
```

## Testing

Tests and python linting can be done using the `tests.sh` script, after running pipenv install:
```
> pipenv run tests/bin/tests.sh
```

## Contributing

To add new dependencies to this repo, it's recommended to do it using [pipenv](https://docs.pipenv.org/en/latest/)
and to follow the below steps:

```
# Install my-package:
pipenv install my-package
# or install my-package as part of the required packages for development (e.g. pytest):
pipenv install my-package --dev
# Sync Pipfile package to setup.py:
pipenv run pipenv-setup sync --dev
# Update the requirements.txt for readthedoc.org because this later does not support pipenv (yet?):                      
pipenv lock --requirements > docs/requirements.txt
```